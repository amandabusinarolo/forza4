/************************************
 *VR443627
 *Amanda Businarolo
 *28/12/2020
 *F4Client.c programma che gestisce
 *i due Client nel gioco forza quattro.
 *************************************/


#include "functions.h"


int main(int argc, char *argv[]){
	char symbol;
	int pid=getpid();
	int gioco_automatico = 0;

	if(argc < 2){
		helpClient();
		exit (EXIT_FAILURE);
	}

	//se viene inserito come parametro * senza apici, la shell lo interpreta come comando e non come un char,
	//quindi ci saranno più di tre argomenti poichè........
	if(argc > 3){
		helpClient();
		exit (EXIT_FAILURE);
	}

	//verifico se l'utente vuole giocare in modo automatico, se sì pongo gioco_automatico = 1
	if(argc==3 && !strncmp(argv[2],"*",1)){
		printf("Questo Client gioca in modo automatico.\n");
		gioco_automatico = 1;
	}

	//inizializzazione
	key = ftok(path, id);
	shm = getSharedMemory(0);

	//collegamento ai semafori creati dal Server
	crea_semafori_client();

	// legge username dal parametro, assegnandolo a username1 o username2
	if(!strlen(shm->username1)){
		mySemNumber = 0;
		// semaforo wait
		sem_wait(mySemNumber);

		// critical section
		strcpy (shm->username1, argv[1]);
		shm->pid1 = pid;
		symbol = shm->symbol1;
		printf("Sono il giocatore con nome %s, simbolo %c \nResto in attesa dell'altro giocatore...\n", argv[1], symbol);

		// semaforo signal
		sem_signal(mySemNumber);
	}
	else if(!strlen(shm->username2)){
		mySemNumber = 1;
		// semaforo wait
		sem_wait(mySemNumber);
		// critical section
		strcpy ( shm->username2, argv[1]);
		shm->pid2 = pid;
		symbol = shm->symbol2;
		printf("Sono il giocatore con nome %s, simbolo %c \n", argv[1], symbol);
		printf("Attesa della mossa dell'altro giocatore...\n");

		// semaforo signal
		sem_signal(mySemNumber);
	}
	else{
		printf("Sono già esistenti due giocatori!");
		exit_client();
	}


	//segnale del Client stesso che ha deciso di terminare per ^C
	signal(SIGINT, segnale_client_ctrlC);

	//segnale da parte del Server che ha deciso di terminare per ^C sul server
	signal(SIGUSR1, segnale_client_sigusr1);

	/* segnale da parte del Server che comunica che l'altro Client
	 *  ha deciso di terminare
	 *  e che egli ha vinto
	 */
	signal(SIGUSR2, segnale_vincita_per_abbandono_altro);

	//Imposto il signal SIGALARM con handler segnale_client_tempo_scaduto, utilizzando sigaction al posto di signal
	//Con la funzione sigaction, alla fine del timeout il programma non resta bloccato sullo scanf
	//e va a settare la colonna scelta a 0
	struct sigaction sa;
	sa.sa_handler = segnale_client_tempo_scaduto;
	sigemptyset(&sa.sa_mask);
	sa.sa_flags = 0;
	sigaction(SIGALRM, &sa, NULL);


	int numcol;
	int check_col;
	int tempo_scaduto=0;
	while(1){

		// semaforo wait
		sem_wait(mySemNumber);

		print_matrix(shm->matrix,shm->rows,shm->cols);

		//controllo se ho vinto precedentemente, ho perso, o sono il primo a giocare, o partita finita alla pari

		if(pid == shm->pid1 && shm->chiHaVinto ){		 //se sono il Client1
			if(shm->chiHaVinto == 1){	//Client1 ha vinto la partita (io ho vinto)
				printf("COMPLIMENTI! HAI VINTO la partita!\n");
			}
			else if(shm->chiHaVinto == 2){	//Client2 ha vinto la partita (io ho perso)
				printf("MI DISPIACE! NON HAI VINTO la partita!\n");
			}
			else if(shm->chiHaVinto == 3){	//matrice piena : partita finita alla pari
				printf("Partita vinta alla pari! La matrice è piena.\n");
			}
			shm->username1[0] = '\0';
			shm->colonnaGiocata = 0;
			sem_signal(mySemNumber);
			exit_client();
		}else if(pid == shm->pid2 && shm->chiHaVinto  ){   //se sono il Client2
			if(shm->chiHaVinto == 1){	//Client1 ha vinto la partita (io ho perso)
				printf("MI DISPIACE! NON HAI VINTO la partita!\n");
			}
			else if(shm->chiHaVinto == 2){	//Client2 ha vinto la partita (io ho vinto)
				printf("COMPLIMENTI! HAI VINTO la partita!\n");
			}
			else if(shm->chiHaVinto == 3){	//matrice piena : partita finita alla pari
				printf("Partita vinta alla pari! La matrice è piena.\n");
			}
			shm->username2[0] = '\0';
			sem_signal(mySemNumber);
			exit_client();
		}

		if(shm->soloStampaMatrice == 1){
			if(tempo_scaduto)
				printf("\nÈ scaduto il tuo tempo per scegliere la colonna! Il turno passa all'altro giocatore.\n");
			printf("Attendi la mossa dell'altro giocatore...\n");
			tempo_scaduto = 0;
			sem_signal(mySemNumber);
			continue;
		}

		//controllo che il giocatore inserisca il gettone nella colonna giusta
		do{
			printf("È il tuo turno. Inserisci il numero della colonna (tempo max %d secondi):" , TIMEOUT_GAME );

			if(gioco_automatico == 1){
				sleep(1);
				srand(time(NULL)); //setta casualmente l'origine della sequenza
				numcol=1+rand()%(shm->cols);//rand() genera un numero casuale tra 1 e il numero di colonne
				check_col = check_matrix_col(shm->matrix,shm->rows,shm->cols,numcol);
				if(!check_col)
					printf("\nLa colonna scelta %d è già piena, devi sceglierne un'altra.\n", numcol);
			}
			else{
				alarm(TIMEOUT_GAME); //attivo timer di tot secondi per digitare la clonna scelta
				fflush(stdin); // pulisce il buffer di input
				if(scanf("%d", &numcol) == 1){
					alarm(0); // annullo il timer
					check_col = check_matrix_col(shm->matrix,shm->rows,shm->cols,numcol);
					if(!check_col)
						printf("\nLa colonna scelta %d è già piena, devi sceglierne un'altra.\n", numcol);
				}
				else
				{
					// va qui quando il timeout è scaduto,
					//imposto a 0 la colonna giocata così il server ignora la mossa
					numcol=0;
					tempo_scaduto = 1;
				}
			}
		}
		while(numcol > shm->cols || !check_col);

		shm->colonnaGiocata = numcol;

		// semaforo signal
		sem_signal(mySemNumber);

	}

	exit_client();
	return EXIT_SUCCESS;
}
