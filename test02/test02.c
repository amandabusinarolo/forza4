/*
 * test02.c
 *
 *  Created on: Apr 17, 2020
 *      Author: amanda
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(void){

	int n, risultato, i;
	printf("inserisci numero:");
	scanf("%i", &n);
	int arr[n+1];
	arr[0]=0;
	arr[1]=1;

	clock_t begin = clock();

	for(i=2; i<n+1; i++)
		arr[i]=arr[i-1]+arr[i-2];

	for(i=0; i<n+1; i++)
			printf(" %i ", arr[i]);

	clock_t end = clock();
	double time_spent = (double)(end - begin) / CLOCKS_PER_SEC * 1000;

	printf("\n tempodi esecuzione in millisecondi= %f", time_spent);

	return 0;
}


