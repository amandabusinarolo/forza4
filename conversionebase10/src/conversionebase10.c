/*
 ============================================================================
 Name        : conversionebase10.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>


int dammi_numero_positivo(char * x){
	int n;
	do {
		printf("Inserisci %s: ", x);
		scanf("%i", &n);
	}
	while (n < 0);

	return n;
}



int lab_11_12_2019(void) {

	srand(time(NULL));

	int n;
	do {
		printf("Inserisci n: ");
		scanf("%i", &n);
	}
	while (n < 0);

	int arr[n];
	int z=0;

	for (int pos = 0; pos < n; pos++) {
		int trovato=0;
		do {
			z=1+rand() % n;
			trovato=0;
			for(int i=0; i<=pos-1; i++)
				if(arr[i]==z){
					trovato=1;
					break;
				}
		}
		while(trovato==1);
		arr[pos]=z;

	}

	for (int pos = 0; pos < n; pos++)
		printf("%i ", arr[pos]);
	printf("\n");


  return 0;
}


// ***********************************************************+
int  minimo(int a , int b){
	if (a<=b) return a;
	else return b;
}


int min(int a, int b, int c, int d) {
  // ritorna il minimo fra a, b e c

	int min=minimo(a,b);
	min=minimo(min,c);
	min=minimo(min,d);

	return min;
}

int are_sorted(int a, int b, int c, int d) {
  // ritorna un numero diverso da 0 se a <= b <= c <= d,
  // altrimenti ritorna 0. Ovvero "determina" se a <= b <= c <= d


	return a<=b && b<=c && c<=d;
}

void print_range(char start, char end) {
  // stampa una riga con i caratteri da start ad end inclusi e infine va a capo

	for(int i=start; i<=end; i++)
		printf("%c", i);

	printf("\n");

}

int is_perfect(int n) {
  // determina se n è perfetto (assumendo che n sia positivo),
  // cioè uguale alla metà della somma dei propri divisori positivi
  // ad esempio 6 è perfetto poiché 6 = (1 + 2 + 3 + 6) / 2
}

void print_binary(int n) {
  // stampa n in binario, con le cifre meno significative a sinistra
}

int main2(void) {
  printf("minimo: %i\n", min(-100, -400, -10,-888));
  printf("Sono ordinati?: %i %i\n", are_sorted(3, 8, 7, 9), are_sorted(3, 8, 8, 9));
  print_range('f', 'w');
  print_range('a', 'z');
  print_range('0', '9');
  print_range('A', 'z');

  printf("%i\n", is_perfect(8128));
  print_binary(8128);



	for(int i=1; i<=254; i++)
		printf("%c", i);

  return 0;
}



// *************************************************************************************



int pari_davanti(void) {

	int n;

	do{
		printf("Inserisci n: ");
		scanf("%i", &n);
	}
	while(n<0);

	int arr[n];
	int r=0;
	int trovato;
	srand(time(NULL));



	for(int i=0; i<n; i++){

		do{
			r = 1 + rand() % n;
			trovato=0;
			for(int k=0; k<i; k++){
				if(r==arr[k]){
					trovato=1;
					break;
				}
			}
		}
		while(trovato==1);
		arr[i]=r;
	}



	int k=0;
	int temp=0;


	for(int i=0; i<n; i++){
		if(arr[i] % 2 == 0){
			temp=arr[k];
			arr[k++]=arr[i];
			arr[i]=temp;
		}
	}


	for(int i=0; i<n; i++){
		if(arr[i] % 2 != 0){
			temp=arr[k];
			arr[k++]=arr[i];
			arr[i]=temp;
		}
	}

	for(int i=0; i<n; i++)
		printf("%i ", arr[i]);

	printf("\n");


	return EXIT_SUCCESS;
}














int pari_e_dispari(void) {

	int n;

	do{
		printf("Inserisci n: ");
		scanf("%i", &n);
	}
	while(n<=0);

	int arr[n];
	srand(time(NULL));

	for(int i=0; i<n; i++){
		arr[i]= 1+ rand() % 1000;
		printf("arr[%i]:%i \n", i, arr[i]);
	}

	int pari[n];
	int dispari[n];
	int resto;
	int z=0;
	int k=0;

	for(int i=0; i<n; i++){
		pari[i]=0;
		dispari[i]=0;
		resto = arr[i] % 2;
		if(resto == 0)
			pari[z++]=arr[i];
		else
			dispari[k++]=arr[i];


	}

	printf("\nI numeri pari sono: ");

	for(int i=0; i<n; i++)
		if(pari[i] != 0)
			printf("%i ", pari[i]);

	printf("\nI numeri dispari sono: ");

	for(int i=0; i<n; i++)
		if(dispari[i] != 0)
			printf("%i ", dispari[i]);


	return EXIT_SUCCESS;
}








int numeri_doppi(void) {

	int n;

	do{
		printf("Inserisci n: ");
		scanf("%i", &n);
	}
	while(n<=0);

	int arr[n];

	for(int i=0; i<n; i++){
		printf("arr[%i]: ", i);
		scanf("%i", &arr[i]);
	}



	int doppi[n];
	int i_doppi=0;

	for(int i=0; i<n; i++){
		doppi[i]=0;
		printf("%i  ", arr[i]);
	}


	printf("\nI numeri doppi sono:");


	for(int i=0; i<n; i++)
		for(int z=i+1; z<n; z++)
			if(arr[i]==arr[z]){

				int esistente = 0;

				for(int k=0; k<i_doppi; k++)
					if(arr[i]==doppi[k])
						esistente = 1;

				if( esistente == 0 )
					doppi[i_doppi++]=arr[i];
			}

	for(int i=0; i<n; i++)
		if(doppi[i] != 0)
			printf("%i ", doppi[i]);

	return EXIT_SUCCESS;
}



int da_decimale_a_binario(void) {

	int numero = 80;
	int base = 8;
	int arr[100] = {99};
	int quoziente, resto;
	int i=0;

	for(int i=0; i<100; i++)
		arr[i] = 99;

	do {
		quoziente = numero / base;
		resto = numero % base;
		arr[ i ] = resto;
		i++;
		printf("%i   %i \n", quoziente, resto);
		numero = quoziente;
	}
	while(quoziente!=0 );

	for(int i=99; i>=0; i--)
		if(arr[i]!=99)
			printf("%i ", arr[ i ]);


	return EXIT_SUCCESS;

}


void print_array(int arr[], int length) {
	for (int pos = 0; pos < length; pos++)
		printf("%i ", arr[pos]);
	printf("\n");
}

void call(int x[], int length) {
	for (int pos = 0; pos < length; pos++)
		x[pos] = x[pos] * 2;

	printf("alla fine della call x contiene: ");
	print_array(x, length);
}

int inserisci_numeri(void) {

	int n1 = dammi_numero_positivo("n1");
	int n2 = dammi_numero_positivo("n2");
	int n3 = dammi_numero_positivo("n3");

	printf("I tre numeri sono: %i %i %i ", n1, n2, n3);

}


int main222(void) {
	int v[] = { 13, -2, 8, 13, 11, 20, -6 };
	printf("prima della chiamata v contiene: ");
	print_array(v, 7);
	call(v, 7);
	printf("dopo la chiamata v contiene: ");
	print_array(v, 7);

	return 0;
}



int main(void) {


	int n;
	do {
		printf("Inserisci n: ");
		scanf("%i", &n);
	}
	while (n < 0);


	return 0;
}


