/************************************
 *VR443627
 *Amanda Businarolo
 *28/12/2020
 *functions.h contenente le funzioni,
 *variabili globali, struttura,
 *usate da F4Server e F4Client
 *************************************/


#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <errno.h>
#include <signal.h>
#include <unistd.h>
#include <time.h>

#define SYMBOL_NOT_INSERTED 0 // significa che la pedina non è inseribile perchè la colonna della matrice di gioco è piena
#define SYMBOL_INSERTED 1 // significa che la pedina è stata inserita correttamente ma il giocatore NON HA vinto
#define SYMBOL_INSERTED_PLAYER_WINNER 2 // significa che la pedina è stata inserita correttamente e il giocatore HA vinto
#define MATRIX_COMPLETE 3 // significa che la matrice di gioco è piena, la partita termina con entrambi gli utenti vincitori
#define MATRIX_MAX_ROWS 12
#define MATRIX_MAX_COLS 12
#define MATRIX_CELS MATRIX_MAX_ROWS*MATRIX_MAX_COLS
#define clear() printf("\033[H\033[J")
#define TIMEOUT_GAME 10
#define PROJ_ID 234

typedef struct Shared_Memo{

	int colonnaGiocata;
	int chiHaVinto; // 0 = nessuno, 1 = primo giocatore, o 2 = secondo giocatore, 3 = entrambi (matrice piena)
	int vincita_per_abbandono; // 1 = si , 0 = no
	int soloStampaMatrice; // se 1 il client stampa la matrice ma non chiede ka mossa

	int pidServer;

	int pid1;
	char username1[20];
	char symbol1;

	int pid2;
	char username2[20];
	char symbol2;

	int rows;
	int cols;
	char matrix[MATRIX_CELS];//vettore di tipo char

} Shared_Memo;

//variabili globali
char *path = "/tmp";
int id = PROJ_ID;
int key;
int shmid;
int n_ctrlc=0;
int mySemNumber;
int sem;
Shared_Memo *shm = NULL;

void print_matrix(char *shared_matrix, int rows, int cols);
void reset_matrix(char *shared_matrix, int rows, int cols);
int complete_matrix(char *shared_matrix, int rows, int cols);
int win_matrix(char *shared_matrix, int rows, int cols, char symbol);
int set_matrix(char *shared_matrix, int rows, int cols, char symbol, int column);
void helpServer();
void helpClient();
void exit_server();
void exit_client();
void segnale_client_sigusr1(int sig);
void segnale_server_ctrl_C(int sig);
void segnale_server_from_client2_to_client1(int sig);
void segnale_server_from_client1_to_client2(int sig);
void segnale_vincita_per_abbandono_altro(int sig);
void segnale_client_ctrlC(int sig);
Shared_Memo* getSharedMemory();
void crea_semafori();
void crea_semafori_client();
void sem_wait(int semNum);
void sem_signal(int semNum);
void segnale_client_tempo_scaduto(int sig);


/**
 *Funzione che chiude il programma F4Serve.c
 */
void exit_server() {

	//scollega il segmento di memoria condivisa situato all'indirizzo specificato da shmaddr
	if( shmdt(shm) == -1) {
		perror("shmdt");
		exit(EXIT_FAILURE);
	}
	//Dealloca il segmento di memoria condivisa
	shmctl(shmid, IPC_RMID, NULL);

	//chiudere i due semfori
	semctl(sem,0,IPC_RMID);
	semctl(sem,1,IPC_RMID);

	printf("\nFINE DELLA PARTITA\n");
	exit( EXIT_SUCCESS );
}

/**
 *Funzione che chiude il programma F4Client.c
 *Shmdt() scollega il segmento di memoria condivisa situato all'indirizzo specificato da shmaddr
 */
void exit_client() {
	if( shmdt(shm) == -1) {
		perror("shmdt");
		exit(EXIT_FAILURE);
	}
	exit( EXIT_SUCCESS );
}

/**
 * Funzione che crea 2 semafori
 */
void crea_semafori_server(){
	sem = semget(key,2,IPC_CREAT | 0666);
	if(sem<0){
		perror("Errore: ");
		exit(1);
	}
}

/**
 * Funzione che collega il client ai semafori del server
 */
void crea_semafori_client(){
	sem = semget(key,2,0666);
	if(sem<0){
		perror("Errore: ");
		exit(1);
	}
}

/**
 *Funzione che decrementa di 1 il semaforo passato come parametro
 */
void sem_wait(int semNum){
	struct sembuf semOperation;
	semOperation.sem_op = -1;
	semOperation.sem_num = semNum;
	semOperation.sem_flg = 0;
	semop(sem, &semOperation, 1);
}

/*
 *Funzione che incrementa di 1 il semaforo passato come parametro
 */
void sem_signal(int semNum){
	struct sembuf semOperation;
	semOperation.sem_op = 1;
	semOperation.sem_num = semNum;
	semOperation.sem_flg = 0;
	semop(sem, &semOperation, 1);
}


/**
 * Funzione da attivare alla ricezione del segnale ^C digitato dal Server
 */
void segnale_server_ctrl_C(int sig){

	int correct1;
	int correct2;
	printf("\nHo ricevuto il segnale ^C di terminazione della partita.\n");
	n_ctrlc++; //variabile globale inizializzata a 0, ogni volta che premo ctrl-c aumenta

	if(n_ctrlc==1){
		printf("Alla prossima pressione di ^C il programma termina\n");
	}
	else{
		if(strlen(shm->username1)){
			correct1=kill(shm->pid1,SIGUSR1); //invia a Client1 il segnale SIGUSR1 di terminazione del gioco, deciso da Server
			if(correct1==-1)
				printf("Segnale non inviato correttamente al client1\n");
		}
		if(strlen(shm->username2)){
			correct2=kill(shm->pid2,SIGUSR1); //invia a Client2 il segnale SIGUSR1 di terminazione del gioco, deciso da Server
			if(correct2==-1)
				printf("Segnale non inviato correttamente al client2\n");
		}
		exit_server();
	}
}

/*
 * Funzione da attivare alla ricezione del segnale SIGUSR1 da parte del Server che intende terminare la partita
 */
void segnale_client_sigusr1(int sig){
	printf("\nFINE del gioco! Il server ha deciso di terminare la partita.\n");
	exit_client();
}

/**
 * Funzione da attivare alla ricezione da parte dello stesso Client che ha deciso di terminare premendo ^C
 * invia al Server il segnale:
 * o SIGUSR1 (se è Client1)
 * o SIGUSR2 (se è Client2)
 * per avvisarlo dell'avenuta decisione di terminare il gioco.
 * Il server invierà un altro segnale all'altro Client avvisandolo che il gioco terminerà e che vince lui automaticamente
 */
void segnale_client_ctrlC(int sig){
	int comando;
	int mypid = getpid();

	printf("\nHai deciso di abbandonare il gioco.\n");

	if(mypid == shm->pid1){
		comando=kill(shm->pidServer,SIGUSR1);
		if(comando==-1)
			printf("\nSegnale non inviato correttamente al server\n");
	}
	else{
		comando=kill(shm->pidServer,SIGUSR2);
		if(comando==-1)
			printf("\nSegnale non inviato correttamente al server\n");
	}
	exit_client();
}


/**
 * Funzione da attivare nel Server alla ricezione del segnale SIGUSR1 mandato da Client1
 * indica che vuole terminare il gioco
 * invia all'altro client il segnale SIGUSR2 per avvisarlo dell'avenuta decisione di terminare il gioco
 */
void segnale_server_from_client1_to_client2(int sig){
	printf("Ho ricevuto il segnale %d di terminazione del gioco dal giocatore 1.\n", sig);
	printf("Il giocatore 2 ha vinto per abbandono del giocatore 1.\n");
	int comando=kill(shm->pid2,SIGUSR2); //mando al Client2 il segnale SIGUSR2 di terminazione, deciso da Client1
	if(comando==-1)
		printf("Segnale non inviato correttamente al Client2\n");

	exit_server();
}

/**
 * Funzione da attivare nel Server alla ricezione del segnale SIGUSR1 mandato da Client2
 * indica che vuole terminare il gioco
 * invia all'altro client il segnale SIGUSR2 per avvisarlo dell'avenuta decisione di terminare il gioco
 */
void segnale_server_from_client2_to_client1(int sig){
	printf("Ho ricevuto il segnale %d di terminazione del gioco dal giocatore 2.\n", sig);
	printf("Il giocatore 1 ha vinto per abbandono del giocatore 2.\n");
	int comando=kill(shm->pid1,SIGUSR2); //mando al Client1 il segnale SIGUSR2 di terminazione, deciso da Client2
	if(comando==-1)
		printf("Segnale non inviato correttamente al Client1\n");

	exit_server();
}


/**
 * Funzione da attivare nel Client, alla ricezione, da parte del Server del segnale SIGUSR2.
 * Il server gli invia il segnale SIGUSR2 che indica che l'altro giocatore ha deciso di abbandonare.
 * Quindi vince per abbandono dell'altro giocatore ed esce.
 */
void segnale_vincita_per_abbandono_altro(int sig){
	printf("\nHAI VINTO! Per abbandono dell'altro giocatore!\n");
	exit_client();
}


/**
 *Funzione che viene attivata alla ricezione del segnale
 *SIGALRM.
 */
void segnale_client_tempo_scaduto(int sig){

}

/**
 *Funzione che stampa la matrice completa
 */
void print_matrix(char *shared_matrix, int rows, int cols){

	clear();
	for(int i=0; i<rows; i++){
		printf("|");
		for(int j=0; j<cols; j++){
			printf(" %c |", *(shared_matrix+(i*cols+j)));
		}
		printf("\n");
	}
}

/**
 *Funzione che imposta su tutte le caselle di gioco
 * il carattere " ". Quindi inizializza la matrice.
 */
void reset_matrix(char *shared_matrix, int rows, int cols){
	for(int i=0; i<rows; i++){
		for(int j=0; j<cols; j++){
			*(shared_matrix+(i*cols+j)) = ' ';
		}
	}
}

/**
 *Funzione che controlla se la matrice è piena. Ritorna:
 *0 se NON piena
 *1 se piena
 */
int complete_matrix(char *shared_matrix, int rows, int cols){
	for(int i=0; i<rows*cols; i++){
		if( *(shared_matrix+i) == ' ')
			return 0;
	}
	return 1;
}

/**
 * Funzione che ritorna:
 * 2 se il giocatore con pedina symbol ha vinto
 * 1 altrimenti
 */
int win_matrix(char *shared_matrix, int rows, int cols, char symbol){

	//controllo per riga
	for(int i=rows-1; i>=0; i--){
		for(int j=0; j<cols-3; j++){
			if(*(shared_matrix+(i*cols+j)) == symbol && *(shared_matrix+(i*cols+j+1)) == symbol && *(shared_matrix+(i*cols+j+2)) == symbol && *(shared_matrix+(i*cols+j+3)) == symbol){
				return SYMBOL_INSERTED_PLAYER_WINNER;
			}
		}
	}

	//controllo per colonna
	for(int i=0; i<(rows-3)*cols; i++){
		if(*(shared_matrix+(i)) == symbol && *(shared_matrix+(i+cols)) == symbol && *(shared_matrix+(i+cols*2)) == symbol && *(shared_matrix+(i+cols*3)) == symbol){
			return SYMBOL_INSERTED_PLAYER_WINNER;
		}
	}

	//controllo per diagonale verso destra
	for(int i=rows-3; i>=0; i--){
		for(int j=0; j<cols-3; j++){
			if(*(shared_matrix+(i*cols+j)) == symbol && *(shared_matrix+(i*cols+j+(cols+1))) == symbol && *(shared_matrix+(i*cols+j+(cols+1)*2)) == symbol && *(shared_matrix+(i*cols+j+(cols+1)*3)) == symbol){
				return SYMBOL_INSERTED_PLAYER_WINNER;
			}
		}
	}

	//controllo per diagonale verso sinistra
	for(int i=rows-3; i>=0; i--){
		for(int j=3; j<cols; j++){
			if(*(shared_matrix+(i*cols+j)) == symbol && *(shared_matrix+(i*cols+j+(cols-1))) == symbol && *(shared_matrix+(i*cols+j+(cols-1)*2)) == symbol && *(shared_matrix+(i*cols+j+(cols-1)*3)) == symbol){
				return SYMBOL_INSERTED_PLAYER_WINNER;
			}
		}
	}

	return SYMBOL_INSERTED;
}

/**
 * Funzione che controlla se la colonna giocata è libera, ritorna:
 * 0 se la colonna è piena
 * 1 se la colonna accetta altri gettoni
 */
int check_matrix_col(char *shared_matrix, int rows, int cols, int column){

	int col_ok=0;
	int result=0;
	column = column - 1;
	for(int i=rows-1; i>=0; i--){
		if( *(shared_matrix+(i*cols+column)) == ' '){
			col_ok=1;
			break;
		}
	}
	return col_ok;
}


/**
 * Funzione che imposta la pedina alla colonna 'column' e ritorna :
 * 1 gettone inserito correttamente ma NON HA vinto
 * 2 gettone inserito correttamente e HA vinto
 * 3 matrice piena : partita finita alla pari
 */
int set_matrix(char *shared_matrix, int rows, int cols, char symbol, int column){

	int result=0;
	column = column - 1;
	for(int i=rows-1; i>=0; i--){
		if( *(shared_matrix+(i*cols+column)) == ' '){
			*(shared_matrix+(i*cols+column)) = symbol;
			break;
		}
	}

	// controlla se il giocatore con l'ultima mossa ha vinto
	result = win_matrix(shared_matrix, rows, cols, symbol);

	// se il giocatore NON ha vinto allora controllo che la matrice non sia completa
	if (result==SYMBOL_INSERTED){

		int piena = complete_matrix(shared_matrix, rows, cols);
		if (piena)
			return MATRIX_COMPLETE;
	}

	return result;
}


/**
 *Funzione chiamata quando lanciando il programma F4Client,
 * non si inserisce come parametro da riga di comando un username.
 * Suggerisce il funzionamento del Client automatico
 */
void helpClient(){
	printf("Usage: F4Client username\nEnter a username!\n ");
	printf("\nExample: ./F4Server Pippo\n");
	printf("\nSe vuoi giocare in automatico con l'altro Client aggiungi '*', CON APICI: ./F4Server Pippo '*'\n");
}


/**
 *Funzione chiamata quando lanciando il programma F4Server,
 * NON si inserisce come parametro da riga di comando:
 * o nessun parametro (righe, colonne, i due simboli di gioco)
 * o i due simboli di gioco
 */
void helpServer(){
	printf("Usage: F4Server rows cols symbol1 symbol2\n");
	printf("rows: Number of rows (min 5, max %d)\n", MATRIX_MAX_ROWS);
	printf("cols: Number of columns (min 5, max %d)\n", MATRIX_MAX_COLS);
	printf("symbol1: simbol char of the first player (A..Z or a..z or 0..9)\n");
	printf("symbol2: simbol char of the second player (A..Z or a..z or 0..9)\n");
	printf("\nExample: ./F4Server 5 6 O X \n");
}

/**
 *Funzione che crea la memoria condivisa,
 *chiamata da F4Server passando come parametro 1
 *o da Client passando come parametro 0.
 */
Shared_Memo* getSharedMemory(int sono_server){

	//crea una nuova area di memoria
	if ( sono_server )
		shmid = shmget(key, sizeof(Shared_Memo), 0666 | IPC_CREAT);
	else
		shmid = shmget(key, sizeof(Shared_Memo), 0666);

	if(shmid == -1){
		if ( sono_server )
			perror("Errore shmget di creazione della memoria condivisa");
		else
			perror("Sei sicuro che sia acceso il server?\nMemoria condivisa non ancora creata dal server");
		exit(1);
	}

	//si collega l'area di memoria all'area dati dei processi che vogliono utilizzarla
	Shared_Memo *shared_memory;
	shared_memory=(Shared_Memo *)shmat(shmid,NULL,0);
	if( !shared_memory ){
		perror("Errore shmat della memoria condivisa");
		exit(1);
	}

	return shared_memory;
}
