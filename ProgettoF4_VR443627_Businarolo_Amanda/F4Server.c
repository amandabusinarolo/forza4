/************************************
*VR443627
*Amanda Businarolo
*28/12/2020
*F4Server.c programma che gestisce
*il Server nel gioco forza quattro.
*************************************/

#include "functions.h"



int main(int argc, char *argv[]){

	if(argc != 5){
		helpServer();
		exit (1);
	}

	int rows = atoi(argv[1]);
	int cols = atoi(argv[2]);
	char symbol1 = *argv[3];
	char symbol2 = *argv[4];


	if(rows<5 || cols<5 || rows*cols > MATRIX_CELS ||(symbol1 == symbol2)){
		helpServer();
		exit(1);
	}

	//da togliere
	printf("Righe: %d, Colonne: %d, Simbolo1: %c, Simbolo2: %c\n", rows, cols, *argv[3], *argv[4]);


	//inizializzazione
	key = ftok(path, id);
	shm = getSharedMemory(1);
	reset_matrix(shm->matrix, rows, cols);

	shm->pidServer = getpid();
	shm->symbol1 = symbol1;
	shm->symbol2 = symbol2;
	shm->chiHaVinto = 0; //pedina inserita correttamente ma non ha vinto
	shm->vincita_per_abbandono = 0;
	shm->colonnaGiocata = 0;
	shm->rows = rows;
	shm->cols = cols;

	// Se premo due volte CTRL^C esce dal programma
	signal(SIGINT, segnale_server_ctrl_C);
	signal(SIGUSR1, segnale_server_from_client1_to_client2);
	signal(SIGUSR2, segnale_server_from_client2_to_client1);

	//creazione dei 2 semafori
	crea_semafori_server();

	//inserimento degli username nella memoria, appena un Client si connette
	printf("ATTESA della connessione del primo giocatore...\n");
	while(!strlen(shm->username1)){
		sem_signal(0);
		sem_wait(0);
		sleep(1);
	}
	printf("Primo giocatore connesso con nome %s...\n", shm->username1);


	printf("ATTESA della connessione del secondo giocatore...\n");
	while(!strlen(shm->username2)){
		sem_signal(1);
		sem_wait(1);
		sleep(1);
	}
	printf("Secondo giocatore connesso con nome %s...\n",shm->username2);


	//arbitro i Client e carico i dati nella matrice
	int risultato=0;
	int giocatore=0;
	int fatto_primo_ctrl_C = 0; //per testare se è già stato premuto un ^C dal Server
	while(1){

		//controllo se i due client hanno terminato il gioco
		if((!strlen(shm->username2))&&(!strlen(shm->username2))){
			printf("I due giocatori hanno terminato la partita.\n");
			exit_server();
		}

		//ad ogni mossa la variabile viene messa a 0,
		//incrementata successivamente a 1 dopo l'inserimento della colonna
		//verrà letta dal client nella memo condivisa.
		//se 0 non stampa la matrice
		//se 1 stampa la matrice aggiornata
		shm->soloStampaMatrice = 0;

		printf("Ora gioca il giocatore numero %d...\n", giocatore);
		sem_signal(giocatore);

		printf("Attendo la mossa dal giocatore numero %d...\n", giocatore);
		sem_wait(giocatore);

		//per evitare che il Client faccia due sem_wait(giocatore) nel caso in cui
		//il server continui il gioco dopo il primo ^C
		if(n_ctrlc == 1 && fatto_primo_ctrl_C == 0){
			sem_wait(giocatore);
			fatto_primo_ctrl_C = 1;
		}

		printf("Carico la matrice con la colonna giocata %d del giocatore numero %d...\n", shm->colonnaGiocata, giocatore);

		//imposta il gettone (del Client 1 o 2) nella colonna scelta dal giocatore
		risultato=0;
		if(shm->colonnaGiocata != 0){
			if(giocatore==0){ //se è il primo giocatore carico il gettone con symol1
				risultato = set_matrix(shm->matrix,shm->rows,shm->cols,shm->symbol1,shm->colonnaGiocata);
				shm->colonnaGiocata=0;
			}
			else{ //se è il secondo giocatore carico il gettone con symol1
				risultato = set_matrix(shm->matrix,shm->rows,shm->cols,shm->symbol2,shm->colonnaGiocata);
				shm->colonnaGiocata=0;
			}
		}

		//controlla se la mossa è stata vincente o partita finita alla pari

		if(risultato==2 && giocatore==0)	//pedina inserita correttamente da Client1 e HA vinto
			shm->chiHaVinto = 1;
		else if(risultato==2 && giocatore==1)	//pedina inserita correttamente da Client2 e HA vinto
			shm->chiHaVinto = 2;
		else if(risultato==3)					//matrice piena : partita finita alla pari
			shm->chiHaVinto = 3;

		//stampa l'aggiornamento del gettone

		//printf("Stampa aggiornamento gettone\n", giocatore);
		shm->soloStampaMatrice = 1;
		sem_signal(giocatore);
		sem_wait(giocatore);

		giocatore++; //passa all'altro giocatore

		if(giocatore==2)
			giocatore=0;

	}

	exit_server();

	return EXIT_SUCCESS;

}

